	//intiate a new angular app
	var app = angular.module('food-agency-ratings', []);

	/* A notifier service called when a user selects a new local authority
	*  The notifier service passes the selected authority to the newAuthoritySelected 
	*  function in the establishments controller. 
	*/
	app.factory('notify', ['$rootScope', function($rootScope) {

		return function(authority) {
			$rootScope.$broadcast('newAuthoritySelected', {
				data: authority
			});
		}

	}]);