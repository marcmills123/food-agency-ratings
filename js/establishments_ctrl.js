	app.controller('establishmentsCtrl',['$scope', '$http', function($scope, $http) {
		$scope.establishments = [];
		$scope.establishments_count = 0;
		$scope.ratings = {};
		$scope.loading = false;
		$scope.have_establishments = false;
		$scope.local_authority;
		$scope.showError = false;

		//Fetch the establishments from the selected local authority
		$scope.getEstablishments = function(local_authority_id) {
			$scope.loading = true;
			$http.get("http://api.ratings.food.gov.uk/Establishments?localAuthorityId="+local_authority_id, { headers: {
				'x-api-version': 2 }
			})
			.then(function successCallback(response) {
				$scope.loading = false;
				$scope.establishments = response.data.establishments;
				$scope.establishments_count = $scope.establishments.length;
				$scope.checkEstablishments();
				$scope.addRatings();
			}, function errorCallback(response) {	
				$scope.loading = false;
				$scope.showError = true;
			});
		}

		//Loop through the establishments and calculate the totals for each rating
		$scope.addRatings = function(){
			angular.forEach($scope.establishments, function(value, key) {
				if (value.RatingKey in $scope.ratings) {
					var rating = $scope.ratings[value.RatingKey];
					rating.value ++;
				}else{
					$scope.ratings[value.RatingKey] = { 'rating_key': value.RatingValue, 'value': 1 };
				}
			});
		}

		//validate that the array of establishments is greater than 0
		$scope.checkEstablishments = function(){

			if($scope.establishments_count > 0){
				$scope.have_establishments = true;
			}else{
				$scope.have_establishments = false;
			}
		}

		//reset the model values
		$scope.reset = function(){
			$scope.establishments = [];
			$scope.establishments_count = 0;
			$scope.checkEstablishments();
			$scope.ratings = {};
			$scope.local_authority; 
		} 

		//function to firing when a user changes the local authority select box
		//intiates the get establishments method
		$scope.$on('newAuthoritySelected', function(event, data) {
			$scope.reset();
			authority = data.data;
			$scope.local_authority = authority.Name;
			$scope.getEstablishments(authority.LocalAuthorityId);
		})

	}]);

