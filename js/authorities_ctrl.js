	app.controller('authoritiesCtrl',['$scope', 'notify', '$http', function($scope, notify, $http) {
		$scope.loadingAuth = true;
		$scope.foundAuth = false;
		$scope.showError = false;


		//call the notify service as a new local authority has been selected
		$scope.update = function(authority) {
			notify(authority);
		}

		//collect the latest list of local authorities from the API
		$scope.fetchAuthorities = function(){
			$http.get('http://api.ratings.food.gov.uk/authorities',{headers: {
				'x-api-version': 2 }
			})
			.then(function successCallback(response) {
				$scope.loadingAuth = false;
				$scope.foundAuth = true;
				$scope.authorities = response.data.authorities;
			}, function errorCallback(response) {
				$scope.loadingAuth = false;
				$scope.showError = true;
			});	
		}

		//on load call fetch method
		$scope.fetchAuthorities();	
	}]);

