module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
  	pkg: grunt.file.readJSON('package.json'),
   // CONFIG ===================================/
   watch: {
   	compass: {
   		files: ['**/*.{scss,sass}'],
   		tasks: ['compass:dev']
   	},
   	js: {
   		files: ['**/*.js'],
   		tasks: ['uglify']
   	}
   },


   compass: {
   	dev: {
   		options: {              
   			sassDir: ['sass'],
   			cssDir: ['styles/css'],
   			environment: 'development',
        outputStyle: 'compressed',

   		}
   	},
    prod: {
      options: {              
        sassDir: ['sass'],
        fontsDir: ['fonts'],
        cssDir: ['styles/css'],
        environment: 'production',
        outputStyle: 'compressed',
      }
    },
   },
   uglify: {
   	all: {
   		files: {
   			'js/min/main.min.js': [
        'js/lib/jquery.min.js',
        'js/lib/angular.min.js',
   			'js/app.js',
   			'js/authorities_ctrl.js', 
   			'js/establishments_ctrl.js'
   			]
   		}
   	},
   },
});
  // DEPENDENT PLUGINS =========================/

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  // TASKS =====================================/

  grunt.registerTask('default', ['uglify','compass','watch']);

};