Food Hygiene Ratings
====================

Focus
-----

As a user I want to see how food hygiene ratings are distributed by percentage
across a selected Local Authority so that I can understand the profile of
establishments in that authority.



TODO
====


1. Create grunt task to run tests
2. Create tests for api request and calculations


Improvements
-------------
### App

1. Improve performance by linking to a document db such as MongoDB
2. Create a cronjob to fetch latest ratings every 24 hours



### API

1. Link country with local authority to have better filters
